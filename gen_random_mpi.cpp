#include "defs.h"

using namespace std;

static MPI_Win win_rowsIndices, win_endV;

#define MIN_BLACKHOLES_CNT 1000
#define MAX_BLACKHOLES_CNT 5000
#define MAX_BLACKHOLES_FRACTION (1.0 / 100)

/* it's necessary to delete some edges to add blackhole */
void add_edges_for_delete_MPI(graph_t *G, int v, vector< pair<vertex_id_t, vertex_id_t> > &edges_for_delete, int target_blackhole_size)
{
    /* bfs */
    queue<vertex_id_t> q;
    q.push(v);
    set<vertex_id_t> visited_vertices;
    multimap<vertex_id_t, vertex_id_t> edges;
    
    while (!q.empty() && visited_vertices.size() < target_blackhole_size) {
        vertex_id_t u = q.front();
        visited_vertices.insert(u);
        q.pop();
        
        pair<vertex_id_t *, unsigned> remote_edges_info = get_remote_edges(G, u, win_rowsIndices, win_endV);
        
        for (int i = 0; i < remote_edges_info.second; i++) {
            edges.insert(make_pair(u, (remote_edges_info.first)[i]));
        }
        
        delete[] remote_edges_info.first;
        
        auto neighbors = edges.equal_range(u);
        for (auto it = neighbors.first; it != neighbors.second; ++it) {
            if (visited_vertices.find(it->second) == visited_vertices.end()) {
                q.push(it->second);
            }
        }
    }
    
    /* add edges for elimination */
    if (visited_vertices.size() == target_blackhole_size) {
        for (auto it = visited_vertices.begin(); it != visited_vertices.end(); ++it) {
            auto neighbors = edges.equal_range(*it);
            for (auto it2 = neighbors.first; it2 != neighbors.second; ++it2) {
                if (visited_vertices.find(it2->second) == visited_vertices.end()) {
                    edges_for_delete.push_back(make_pair(*it, it2->second));
                }
            }
        }
    }
}

/* add some blackholes into the graph */
void add_blackholes_MPI(graph_t *G, int target_blackhole_size)
{
    MPI_Win_create(G->rowsIndices, (G->n / G->nproc + 2) * sizeof(edge_id_t), sizeof(edge_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_rowsIndices);
    MPI_Win_fence(0, win_rowsIndices);
    
    int max_edges_per_node = 0;
    MPI_Allreduce(&G->local_m, &max_edges_per_node, 1, MPI_LONG_LONG_INT, MPI_MAX, MPI_COMM_WORLD);
    
    MPI_Win_create(G->endV, max_edges_per_node * sizeof(vertex_id_t), sizeof(vertex_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_endV);
    MPI_Win_fence(0, win_endV);
    vector< pair<vertex_id_t, vertex_id_t> > edges_for_delete;
    if (G->rank == 0) {
        srand(time(NULL));
        int preferred_blackholes_cnt = MIN_BLACKHOLES_CNT + rand() % (MAX_BLACKHOLES_CNT - MIN_BLACKHOLES_CNT);
        if (preferred_blackholes_cnt > G->n * MAX_BLACKHOLES_FRACTION) {
            preferred_blackholes_cnt = G->n * MAX_BLACKHOLES_FRACTION;
        }
        srand48(time(NULL));
        for (int i = 0; i < G->n; i++) {
            if (drand48() < (preferred_blackholes_cnt * 1.0) / (G->n)) {
                add_edges_for_delete_MPI(G, i, edges_for_delete, target_blackhole_size);
            }
        }
    }
    
    int sz = 0;
    if (G->rank == 0) {
        sz = edges_for_delete.size();
    }
    
    MPI_Bcast(&sz, 1, MPI_INT, 0, MPI_COMM_WORLD);
    edges_for_delete.resize(sz);
    MPI_Bcast(edges_for_delete.data(), edges_for_delete.size(), MPI_2INT, 0, MPI_COMM_WORLD);
    
    /* add edges for elimination */
    unordered_set<unsigned long long> edges_for_delete_set;
    for (int i = 0; i < edges_for_delete.size(); i++) {
        if (VERTEX_OWNER(edges_for_delete[i].first, G->n, G->nproc) == G->rank) {
            edges_for_delete_set.insert((unsigned long long)(edges_for_delete[i].first) << 32 | edges_for_delete[i].second);
        }
    }
    
    /* remain only fine edges - update graph structure */
    edge_id_t ptr = 0;
    for (vertex_id_t i = 0; i < G->local_n; i++) {
        edge_id_t prev_ptr = ptr;
        for (edge_id_t j = G->rowsIndices[i]; j < G->rowsIndices[i + 1]; j++) {
            if (edges_for_delete_set.find((unsigned long long)(VERTEX_TO_GLOBAL(i, G->n, G->nproc, G->rank)) << 32 | G->endV[j]) == edges_for_delete_set.end()) {
                G->endV[ptr++] = G->endV[j];
            }
        }
        G->rowsIndices[i] = prev_ptr;
    }
    
    G->rowsIndices[G->local_n] = ptr;
    G->local_m = ptr;
    MPI_Allreduce(&G->local_m, &G->m, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Win_free(&win_rowsIndices);
    MPI_Win_free(&win_endV);
}

/* distributed random graph generator */
void gen_random_graph_MPI(graph_t *G, int avg_vertex_degree)
{
    /* init */
    vertex_id_t n, local_n;
    edge_id_t local_m, num_dir_edges;
    edge_id_t offset;
    vertex_id_t *permV, tmpVal;
    bool permute_vertices;
    vertex_id_t u, v;
    int seed;
    vertex_id_t *edges;
    vertex_id_t *degree;
    int size, rank, lgsize;
    permute_vertices = true;
    n = G->n;
    unsigned TotVertices;
    TotVertices = G->n;
    
    MPI_Datatype MPI_VERTEX_ID_T;
    MPI_Type_contiguous(sizeof(vertex_id_t), MPI_BYTE, &MPI_VERTEX_ID_T);
    MPI_Type_commit(&MPI_VERTEX_ID_T);
    
    rank = G->rank;
    size = G->nproc;
    for (lgsize = 0; lgsize < size; ++lgsize) {
        if ((1 << lgsize) == size) {
            break;
        }
    }
    
    /* get size of vertices and edges blocks for current processes */
    local_n = get_local_n(G);
    G->local_n = local_n;
    local_m = get_local_m(G, avg_vertex_degree);
    num_dir_edges = local_m;
    G->local_m = local_m;
    edges = new vertex_id_t[2 * num_dir_edges];
    assert(edges != NULL);
    degree = new vertex_id_t[local_n];
    assert(degree != NULL);
    memset(degree, 0, sizeof(vertex_id_t) * local_n);
    
    seed = (int)time(NULL) + rank;
    srand(seed);
    /* generate edges */
    for (edge_id_t i = 0; i < num_dir_edges; i++) {
        vertex_id_t u = rand() % n;
        vertex_id_t v = rand() % n;
        edges[2 * i + 0] = u;
        edges[2 * i + 1] = v;
    }
    
    /* reshuffle */
    /* own random for the same mapping in all nodes */
    if (permute_vertices) {
        permV = new vertex_id_t[n];
        assert(permV != NULL);
        
        my_srand(469762);
        for (vertex_id_t i = 0; i < n; i++) {
            permV[i] = i;
        }
        
        for (vertex_id_t i = 0; i < n; i++) {
            vertex_id_t j = n * my_rand();
            tmpVal = permV[i];
            permV[i] = permV[j];
            permV[j] = tmpVal;
        }
        
        for (edge_id_t i = 0; i < num_dir_edges; i++) {
            edges[2 * i + 0]  = permV[edges[2 * i + 0]];
            edges[2 * i + 1] = permV[edges[2 * i + 1]];
        }
        
        delete[] permV;
    }
    
    vertex_id_t *send_edges = new vertex_id_t[2 * local_m];
    assert(send_edges != NULL);
    vertex_id_t *recv_edges;
    int *send_counts = new int[size];
    assert(send_counts != NULL);
    memset(send_counts, 0, sizeof(int) * size);
    int *recv_counts = new int[size];
    assert(recv_counts != NULL);
    memset(recv_counts, 0, sizeof(int) * size);
    edge_id_t *send_offsets_edge = new edge_id_t[size];
    assert(send_offsets_edge != NULL);
    memset(send_offsets_edge, 0, sizeof(edge_id_t) * size);
    edge_id_t *recv_offsets_edge = new edge_id_t[size];
    assert(recv_offsets_edge != NULL);
    memset(recv_offsets_edge, 0, sizeof(edge_id_t) * size);
    /* calc count of data in each process */
    for (edge_id_t i = 0; i < num_dir_edges; i++) {
        int proc_id = VERTEX_OWNER(edges[2 * i + 0], TotVertices, size);
        send_counts[proc_id]++;
    }
    
    /* calc offsets */
    for (int i = 1; i < size; i++) {
        send_offsets_edge[i] = send_offsets_edge[i - 1] + 2 * send_counts[i - 1];
    }
    
    /* clear send_counts for next using */
    for (int i = 0; i < size; i++) {
        send_counts[i] = 0;
    }
    
    /* copy edges to send_data */
    for (edge_id_t i = 0; i < num_dir_edges; i++) {
        int proc_id = VERTEX_OWNER(edges[2 * i + 0], TotVertices, size);
        offset = send_offsets_edge[proc_id] + 2 * send_counts[proc_id];
        send_edges[offset + 0] = edges[2 * i + 0];
        send_edges[offset + 1] = edges[2 * i + 1];
        send_counts[proc_id]++;
    }
    
    delete[] edges;
    MPI_Request request[size];
    MPI_Status status[size];
    /* report counts to each process */
    MPI_Alltoall(send_counts, 1, MPI_INT, recv_counts, 1, MPI_INT, MPI_COMM_WORLD);
    edge_id_t counts = 0;
    for (int i = 0; i < size; i++) {
        counts += recv_counts[i];
    }
    
    /* calc offsets and number of elements for the next MPI_Send */
    for (int i = 0; i < size; i++) {
        recv_counts[i] = 2 * recv_counts[i];
        send_counts[i] = 2 * send_counts[i]; 
    }
    
    for (int i = 1; i < size; i++) {
        recv_offsets_edge[i] = recv_offsets_edge[i - 1] + recv_counts[i - 1];
    }
    
    recv_edges = new vertex_id_t[2 * counts];
    assert(recv_edges != NULL);
    
    /* send edges to each process */
    for (int i = 0; i < size; i++) {
        MPI_Irecv(&recv_edges[recv_offsets_edge[i]], recv_counts[i], MPI_VERTEX_ID_T, i, G->rank, MPI_COMM_WORLD, &request[i]);
    }
    
    for (int i = 0; i < size; i++) {
        MPI_Send(&send_edges[send_offsets_edge[i]], send_counts[i], MPI_VERTEX_ID_T, i, i, MPI_COMM_WORLD);
    }
    
    MPI_Waitall(size, request, status);
    /* saving new value for local_m */
    local_m = counts;
    G->local_m = local_m;
    
    delete[] send_edges;
    delete[] recv_offsets_edge;
    delete[] send_offsets_edge;
    delete[] recv_counts;
    delete[] send_counts;
    
    for (edge_id_t i = 0; i < 2 * G->local_m; i += 2) {
        degree[VERTEX_LOCAL(recv_edges[i], TotVertices, size, rank)]++;
    }
    
    /* update graph data structure */
    G->endV = new vertex_id_t[G->local_m];
    assert(G->endV != NULL);
    memset(G->endV, 0, sizeof(vertex_id_t) * G->local_m);
    G->rowsIndices = new edge_id_t[local_n + 1];
    assert(G->rowsIndices != NULL);
    G->rowsIndices[0] = 0; 
    for (vertex_id_t i = 1; i <= G->local_n; i++) {
        G->rowsIndices[i] = G->rowsIndices[i - 1] + degree[i - 1];
    }
    
    for (edge_id_t i = 0; i < 2 * G->local_m; i += 2) {
        u = VERTEX_LOCAL(recv_edges[i], TotVertices, size, rank);
        v = recv_edges[i + 1];
        offset = degree[u]--;
        G->endV[G->rowsIndices[u] + offset - 1] = v;
    }
    
    delete[] recv_edges;
    delete[] degree;
}

/* helper */
static void usage(int argc, char **argv)
{
    printf("MPI random graph generator\n");
    printf("Usage:\n");
    printf("%s -s <scale> [other options]\n", argv[0]);
    printf("Options:\n");
    printf("   -s <scale> -- number of vertices is 2^<scale>\n");
    printf("   -k <average vertex degree>\n");
    printf("   -blackhole_size <target_blackhole_size> -- number of vertices in target blackhole subgraph\n");
    printf("   -out <output filename> -- file for the graph storage\n");
    MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
}

/* generate */
void generate(int argc, char **argv, graph_t *G)
{
    char outFilename[FILENAME_LEN];
    MPI_Init(&argc, &argv);
    int scale = -1;
    int avg_vertex_degree = -1;
    int target_blackhole_size = -1;
    
    MPI_Comm_size(MPI_COMM_WORLD, &G->nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &G->rank);
    
    if (argc == 1) {
        usage(argc, argv);
    }
    
	bool no_outFilename = true;
	
    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-s")) {
            scale = (int)atoi(argv[++i]);
            G->n = (vertex_id_t)1 << scale;
        }
        
        if (!strcmp(argv[i], "-k")) {
            avg_vertex_degree = (int)atoi(argv[++i]);
        }
        
        if (!strcmp(argv[i], "-blackhole_size")) {
            target_blackhole_size = (int)atoi(argv[++i]);
        }
        
		if (!strcmp(argv[i], "-out")) {
            no_outFilename = false;
            sprintf(outFilename, "%s", argv[++i]);
        }
    }
    
    if (scale != -1 && target_blackhole_size >= 2 && avg_vertex_degree != -1) {
        G->m = G->n * (edge_id_t)avg_vertex_degree;
        gen_random_graph_MPI(G, avg_vertex_degree);
        add_blackholes_MPI(G, target_blackhole_size);
        if (no_outFilename) {
            sprintf(outFilename, "random-s%d-k%d-tbs%d", scale, avg_vertex_degree, target_blackhole_size);
        }
        write_graph_MPI(G, outFilename);
    } else {
        usage(argc, argv);
    }
}

int main(int argc, char **argv)
{
    graph_t g;
    /* generate */
    generate(argc, argv, &g);
    MPI_Barrier(MPI_COMM_WORLD);
    if (g.rank == 0) {
        cout << "Done" << endl;
    }
    
    //printGraph(&g);
    
    MPI_Finalize();
    return 0;
}
