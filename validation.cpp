#include "defs.h"

char inFilename[FILENAME_LEN];
char resFilename[FILENAME_LEN];

using namespace std;

/* helper */
void usage(int argc, char **argv)
{
    printf("%s\n", argv[0]);
    printf("Usage:\n");
    printf("%s -in <graph filename> -blackhole_size <target_blackhole_size> -res <result>\n", argv[0]);
    printf("Options:\n");
    printf("    -in <graph filename> -- input graph filename\n");
    printf("    -blackhole_size <target_blackhole_size> -- target blackhole size\n");
    printf("    -res <result> -- result filename\n");
    MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
}

/* initialization */
void init(int argc, char **argv, graph_t *G, int &blackhole_size)
{
    MPI_Init(&argc, &argv);
    bool no_inFilename = true;
    bool no_blackholeSize = true;
    bool no_resFilename = true;
    
    inFilename[0] = resFilename[0] = '\0';
    blackhole_size = 0;
    
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-in")) {
            strncpy(inFilename, argv[++i], sizeof(inFilename));
            no_inFilename = false;
            readGraph_singleFile_MPI(G, inFilename);
        }
        
        if (!strcmp(argv[i], "-blackhole_size")) {
            blackhole_size = atoi(argv[++i]);
            no_blackholeSize = false;
        }
        
        if (!strcmp(argv[i], "-res")) {
            strncpy(resFilename, argv[++i], sizeof(resFilename));
            no_resFilename = false;
        }
    }
    
    if (no_inFilename || no_blackholeSize || no_resFilename) {
        usage(argc, argv);
    }
}

string get_str(const vector<vertex_id_t> &vec)
{
    string res = "";
    for (int i = 0; i < vec.size(); i++) {
        res += to_string(vec[i]) + ',';
    }
    
    return res;
}

int main(int argc, char **argv)
{
    graph_t *G = new graph_t;
    int blackhole_size;
    init(argc, argv, G, blackhole_size);
    
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    MPI_Win win_rowsIndices, win_endV;
    
    /* open some info for remote processes */
    MPI_Win_create(G->rowsIndices, (G->n / G->nproc + 2) * sizeof(edge_id_t), sizeof(edge_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_rowsIndices);
    MPI_Win_fence(0, win_rowsIndices);
    
    int max_edges_per_node = 0;
    MPI_Allreduce(&G->local_m, &max_edges_per_node, 1, MPI_LONG_LONG_INT, MPI_MAX, MPI_COMM_WORLD);
    
    MPI_Win_create(G->endV, max_edges_per_node * sizeof(vertex_id_t), sizeof(vertex_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_endV);
    MPI_Win_fence(0, win_endV);
    
    
    if (!rank) {
        int right_blackholes_cnt = 0;
        FILE *f;
        f = fopen(resFilename, "r");
        if (f == NULL) {
            MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
        }
        
        /* read the result */
        vertex_id_t val;
        vector<vertex_id_t> cur_blackhole_ids;
        
        /* keep considered subgraphs */
        unordered_set<string> checked_subgraphs;
        while (fscanf(f, "%d", &val) == 1) {
            if (!(0 <= val && val < G->n)) {
                MPI_Abort(MPI_COMM_WORLD, 1);
            }
            cur_blackhole_ids.push_back(val);
            if (cur_blackhole_ids.size() == blackhole_size) {
                sort(cur_blackhole_ids.begin(), cur_blackhole_ids.end());
                if (checked_subgraphs.find(get_str(cur_blackhole_ids)) == checked_subgraphs.end()) {
                    if (is_black_hole(G, cur_blackhole_ids, win_rowsIndices, win_endV)) {
                        right_blackholes_cnt++;
                    } else {
                        // it isn't blackhole, sorry
                        MPI_Abort(MPI_COMM_WORLD, 1);
                    }
                    checked_subgraphs.insert(get_str(cur_blackhole_ids));
                }
                
                cur_blackhole_ids.clear();
            }
        }
        
        fclose(f);
        
        cout << "have found " << right_blackholes_cnt << " blackholes" << endl;
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    delete G;
    
    MPI_Finalize();
    
    return 0;
}
