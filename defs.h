#pragma once

#include <mpi.h>
#include <cstdio>
#include <cstdint>
#include <cstdbool>
#include <ctime>
#include <sys/types.h>
#include <iostream>
#include <queue>
#include <cstring>
#include <cfloat>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <utility>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <memory>
#include <unordered_set>
#include <string>
#include <cstdlib>
#include <thread>
#include <chrono>

#define FILENAME_LEN 256
#define eps 1e-6

typedef unsigned vertex_id_t;
typedef unsigned long long edge_id_t;

/* The graph data structure*/
typedef struct
{
    /***
     The minimal graph repesentation consists of:
     n        -- the number of vertices
     m        -- the number of edges
     endV     -- an array of size m that stores the
                 destination ID of an edge <src->dest>.
     rowsIndices -- an array of size (n + 1) that stores pointers to the endV array (CRS format).
                 The degree of vertex i is given by rowsIndices[i + 1] - rowsIndices[i], and the
                 edges out of i are stored in the contiguous block
                 endV[rowsIndices[i] .. rowsIndices[i + 1] - 1].
     Vertices are numbered from 0 in our internal representation.
     ***/
    vertex_id_t n;
    edge_id_t m;
    edge_id_t *rowsIndices;
    vertex_id_t *endV;
    
    /* Distributed version variables */
    int nproc, rank;
    vertex_id_t local_n; /* local vertices number */
    edge_id_t local_m; /* local edges number */
} graph_t;

/* function returns size of edges block for current process */
edge_id_t get_local_m(graph_t *G, int avg_vertex_degree);

/* own random */
double my_rand();
void my_srand(unsigned seed);


/* write graph to file */
void writeGraph(graph_t *G, char *filename);
void write_graph_MPI(graph_t *G, char *filename);
void write_answer(const std::vector<vertex_id_t> &all_answers_pool, char *outFilename, int target_blackhole_size);

/* read graph from file */
void readGraph(graph_t *G, char *filename);
void readGraph_singleFile_MPI(graph_t *G, char *filename);

/* print graph */
void printGraph(graph_t *G);

/* free graph */
void freeGraph(graph_t *G);

/* algorithm */
int run(graph_t *G, const int &blackHole_target_size, const char * outFilename);

/* check the subgraph is blackhole or not */
bool is_black_hole(graph_t *G, const std::vector<vertex_id_t> &blackhole_vertices_ids, MPI_Win &win_rowsIndices, MPI_Win &win_endV);

/* get vertex info (edges) from remote process */
std::pair<vertex_id_t *, unsigned> get_remote_edges(graph_t *G, vertex_id_t u, MPI_Win &win_rowsIndices, MPI_Win &win_endV);

/* get owner process id of vertex v */
int VERTEX_OWNER(const vertex_id_t v, const vertex_id_t TotVertices, const int size);

/* get local vertex number */
vertex_id_t VERTEX_LOCAL(const vertex_id_t v, const vertex_id_t TotVertices, const int size, const int rank);

/* get global vertex number */
vertex_id_t VERTEX_TO_GLOBAL(const vertex_id_t v_local, const vertex_id_t TotVertices, const int size, const int rank);

/* get local number of vertices */
vertex_id_t get_local_n(graph_t *G);

