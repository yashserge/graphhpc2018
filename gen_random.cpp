#include "defs.h"

char outFilename[FILENAME_LEN];

using namespace std;

#define MIN_BLACKHOLES_CNT 100
#define MAX_BLACKHOLES_CNT 200

#define MAX_BLACKHOLES_FRACTION (1.0 / 15)

/* helper */
void usage(int argc, char **argv)
{
    printf("Random graph generator\n");
    printf("Usage:\n");
    printf("%s -s <scale> [other options]\n", argv[0]);
    printf("Options:\n");
    printf("   -s <scale>, number of vertices is 2^<scale>\n");
    printf("   -k <average vertex degree>\n");
    printf("   -blackhole_size <target_blackhole_size> -- number of vertices in target blackhole subgraph\n");
    printf("   -out <output filename>, file for the graph storage\n");
    exit(1);
}

/* initialization */
void init(int argc, char **argv, graph_t *G, int &target_blackhole_size)
{
	bool no_out_filename = true;
    int scale = -1;
    
    int avg_vertex_degree = -1;
    if (argc == 1) {
        usage(argc, argv);
    }
    
    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-s")) {
            scale = (int)atoi(argv[++i]);
        }
        
        if (!strcmp(argv[i], "-k")) {
            avg_vertex_degree = (int)atoi(argv[++i]);
        }
        
        if (!strcmp(argv[i], "-blackhole_size")) {
            target_blackhole_size = (int)atoi(argv[++i]);
        }
        
		if (!strcmp(argv[i], "-out")) {
            no_out_filename = false;
			sprintf(outFilename, "%s", argv[++i]);
        }
    }
    
    if (scale == -1 || target_blackhole_size < 2 || avg_vertex_degree == -1) {
        usage(argc, argv);
    }
    
	if (no_out_filename) {
    	sprintf(outFilename, "random-s%d-k%d-tbs%d", scale, avg_vertex_degree, target_blackhole_size);
	}
    
    G->n = (vertex_id_t)1 << scale;
    G->m = G->n * avg_vertex_degree;
}

/* it's necessary to delete some edges to add blackhole */
void add_edges_for_delete(int v, vector<vector<pair<vertex_id_t ,edge_id_t>>> &edges, set<edge_id_t> &edge_nums_for_delete, int target_blackhole_size)
{
    /* bfs */
    queue<int> q;
    q.push(v);
    set<int> visited_vertices;
    
    while (!q.empty() && visited_vertices.size() < target_blackhole_size) {
        int u = q.front();
        visited_vertices.insert(u);
        q.pop();
        for (int i = 0; i < edges[u].size(); i++) {
            if (visited_vertices.find(edges[u][i].first) == visited_vertices.end()) {
                q.push(edges[u][i].first);
            }
        }
    }
    
    /* add edges for elimination */
    if (visited_vertices.size() == target_blackhole_size) {
        for (auto it = visited_vertices.begin(); it != visited_vertices.end(); ++it) {
            for (int i = 0; i < edges[*it].size(); i++) {
                if (visited_vertices.find(edges[*it][i].first) == visited_vertices.end() && edges[*it][i].second > 0) {
                    edge_nums_for_delete.insert(edges[*it][i].second - 1);
                }
            }
        }
    }
}

/* random graph generator */
void gen_random_graph(graph_t *G, int target_blackhole_size)
{
    /* init */
    vertex_id_t n;
    edge_id_t m;
    edge_id_t offset;
    bool permute_vertices = true;
    vertex_id_t *permV, tmpVal;
    vertex_id_t u, v;
    vertex_id_t *src;
    vertex_id_t *dest;
    unsigned *degree;
    n = G->n;
    m = G->m;
    src = new vertex_id_t[m];
    assert(src != NULL);
    dest = new vertex_id_t[m];
    assert(dest != NULL);
    degree = new unsigned[n];
    assert(degree != NULL);
    memset(degree, 0, sizeof(unsigned) * n);
    
    srand(time(NULL));
    
    vector<vector<pair<vertex_id_t, edge_id_t>>> edges(n);
    /* generate edges */
    for (edge_id_t i = 0; i < m; i++) {
        vertex_id_t u = rand() % n;
        vertex_id_t v = rand() % n;
        src[i] = u;
        dest[i] = v;
        edges[u].push_back(make_pair(v, i + 1));
        edges[v].push_back(make_pair(u, -i - 1));
    }
    
    srand(time(NULL));
    
    set<edge_id_t> edge_nums_for_delete;
    /* add some blackholes into the graph */
    int preferred_blackholes_cnt = MIN_BLACKHOLES_CNT + rand() % (MAX_BLACKHOLES_CNT - MIN_BLACKHOLES_CNT);
    if (preferred_blackholes_cnt > G->n * MAX_BLACKHOLES_FRACTION) {
        preferred_blackholes_cnt = G->n * MAX_BLACKHOLES_FRACTION;
    }
    
    srand48(time(NULL));
    for (int i = 0; i < n; i++) {
        if (drand48() < (preferred_blackholes_cnt * 1.0) / (n)) {
            add_edges_for_delete(i, edges, edge_nums_for_delete, target_blackhole_size);
        }
    }
    
    /* remain only fine edges */
    edge_id_t ptr = 0;
    for (edge_id_t i = 0; i < m; i++) {
        if (edge_nums_for_delete.find(i) == edge_nums_for_delete.end()) {
            src[ptr] = src[i];
            dest[ptr++] = dest[i];
        }
    }
    
    m = ptr;
    
    /* reshuffle */
    if (permute_vertices) {
        srand48(time(NULL));
        permV = new vertex_id_t[n];
        assert(permV != NULL);
        
        for (vertex_id_t i = 0; i < n; i++) {
            permV[i] = i;
        }
        
        for (vertex_id_t i = 0; i < n; i++) {
            vertex_id_t j = n * drand48();
            tmpVal = permV[i];
            permV[i] = permV[j];
            permV[j] = tmpVal;
        }
        
        for (edge_id_t i = 0; i < m; i++) {
            src[i] = permV[src[i]];
            dest[i] = permV[dest[i]];
        }
        
        delete[] permV;
    }
    
    /* update graph data structure */
    for (edge_id_t i = 0; i < m; i++) {
        degree[src[i]]++;
    }
    
    G->endV = new vertex_id_t[m];
    assert(G->endV != NULL);
    
    G->rowsIndices = new edge_id_t[n + 1];
    assert(G->rowsIndices != NULL);
    
    G->n = n;
    G->m = m;
    
    G->rowsIndices[0] = 0;
    for (vertex_id_t i = 1; i <= G->n; i++) {
        G->rowsIndices[i] = G->rowsIndices[i - 1] + degree[i - 1];
    }
    
    for (edge_id_t i = 0; i < m; i++) {
        u = src[i];
        v = dest[i];
        offset = degree[u]--;
        G->endV[G->rowsIndices[u] + offset - 1] = v;
    }
    
    delete[] src;
    delete[] dest;
    delete[] degree;
}

int main(int argc, char **argv)
{
    graph_t g;
    int target_blackhole_size = 0;
    init(argc, argv, &g, target_blackhole_size);
    gen_random_graph(&g, target_blackhole_size);
    writeGraph(&g, outFilename);
    
    return 0;
}
