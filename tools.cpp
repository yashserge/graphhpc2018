#include "defs.h"

using namespace std;

/* function returns size of edges block for current process */
edge_id_t get_local_m(graph_t *G, int avg_vertex_degree)
{
    return G->local_n * avg_vertex_degree;
}

/* own random */
/* ********************* */
unsigned long long my_next = 1;

double my_rand()
{
    my_next = my_next * 1365781351523LL + 67325;
    return (double)((my_next / (1LL << 32)) % (1LL << 31)) / (1LL << 31);
}

void my_srand(unsigned seed)
{
    my_next = seed;
}
/* ********************* */

/* read graph from the file */
void readGraph(graph_t *G, char *filename)
{
    unsigned char align;
    FILE *f = fopen(filename, "rb");
    assert(f != NULL);
    assert(fread(&G->n, sizeof(vertex_id_t), 1, f) == 1);
    assert(fread(&G->m, sizeof(edge_id_t), 1, f) == 1);
    assert(fread(&align, sizeof(unsigned char), 1, f) == 1);
    G->rowsIndices = new edge_id_t[G->n + 1];
    assert(G->rowsIndices != NULL);
    assert(fread(G->rowsIndices, sizeof(edge_id_t), G->n + 1, f) == (G->n + 1));
    G->endV = new vertex_id_t[G->m];
    assert(G->endV != NULL);
    assert(fread(G->endV, sizeof(vertex_id_t), G->m, f) == G->m);    
    fclose(f);
}

/* write graph to the file */
void writeGraph(graph_t *G, char *filename)
{
    FILE *f = fopen(filename, "wb");
    assert(f != NULL);
    assert(fwrite(&G->n, sizeof(vertex_id_t), 1, f) == 1);
    assert(fwrite(&G->m, sizeof(edge_id_t), 1, f) == 1);
    unsigned char align = 0;
    assert(fwrite(&align, sizeof(unsigned char), 1, f) == 1);
    assert(fwrite(G->rowsIndices, sizeof(edge_id_t), G->n + 1, f) == G->n + 1);
    assert(fwrite(G->endV, sizeof(vertex_id_t), G->m, f) == G->m);
    fclose(f);
}

/* all processes read graph from the file */
void readGraph_singleFile_MPI(graph_t *G, char *filename)
{
    unsigned char align;
    int rank, size;
    MPI_Offset offset, offset_row, offset_col;
    edge_id_t my_edges[2];
    int local_n = 0;
    int local_m = 0;
    int k;
    unsigned TotVertices;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    G->rank = rank;
    G->nproc = size;
    int lgsize; 
    for (lgsize = 0; lgsize < size; ++lgsize) {
        if ((1 << lgsize) == size) {
            break;
        }
    }
    
    MPI_File fh;
    MPI_Status status;    
    MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    if (fh == NULL) {
        MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
    }
    MPI_File_read(fh, &G->n, 1, MPI::UNSIGNED, &status);
    offset = sizeof(vertex_id_t);
    TotVertices = G->n;
    
    MPI_File_read_at(fh, offset, &G->m, 1, MPI::UNSIGNED_LONG_LONG, &status);
    offset += sizeof(edge_id_t);
    
    MPI_File_read_at(fh, offset, &align, 1, MPI::UNSIGNED_CHAR, &status);
    offset += sizeof(unsigned char);
    offset_row = offset;
    
    for (unsigned i = 0; i < G->n; i++) {
        if (rank == VERTEX_OWNER(i, TotVertices, size)) {
            MPI_File_read_at(fh, offset_row + i * sizeof(edge_id_t), &my_edges[0], 2, MPI::UNSIGNED_LONG_LONG, &status);
            local_n++;
            local_m += my_edges[1] - my_edges[0];
        }
    }
    
    G->local_n = local_n;
    G->local_m = local_m;
    offset_col = offset_row + (G->n + 1) * sizeof(edge_id_t);
    
    G->rowsIndices = new edge_id_t[G->local_n + 1];
    assert(G->rowsIndices != NULL);
    G->endV = new vertex_id_t[G->local_m];
    assert(G->endV != NULL);
    G->rowsIndices[0] = 0;
    k = 1;
    
    for (unsigned i = 0; i < G->n; i++) {
        if (rank == VERTEX_OWNER(i, TotVertices, size)) {
            MPI_File_read_at(fh, offset_row + i * sizeof(edge_id_t), &my_edges[0], 2, MPI::UNSIGNED_LONG_LONG, &status);
            G->rowsIndices[k] = G->rowsIndices[k - 1] + my_edges[1] - my_edges[0];
            MPI_File_read_at(fh, offset_col + my_edges[0] * sizeof(vertex_id_t), &G->endV[G->rowsIndices[k - 1]],
                G->rowsIndices[k] - G->rowsIndices[k - 1], MPI::UNSIGNED, &status);
            k++;
        }
    }
    
    MPI_File_close(&fh);
}

/* free graph */
void freeGraph(graph_t *G)
{
    delete[] G->rowsIndices;
    delete[] G->endV;
}

/* print graph */
void printGraph(graph_t *G)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (!rank) {
        cout << "graph:" << endl;
    }
    int trash = 0;
    MPI_Status status;
    if (rank) {
        MPI_Recv(&trash, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
    }
    //srand48(time(NULL));
    for (int i = 0; i < G->local_n; i++) {
        for (int j = G->rowsIndices[i]; j < G->rowsIndices[i + 1]; j++) {
            //if (drand48() < 1e-6) {
                cout << "u = " << VERTEX_TO_GLOBAL(i, G->n, size, rank) << " " << " v = " << G->endV[j] << endl;
            //}
        }
    }
    
    if (rank != size - 1) {
        MPI_Send(&trash, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD);
}

/* write distributed graph */
void write_graph_MPI(graph_t *G, char *filename)
{
    FILE *f;
    
    unsigned long long shift = 0;
    MPI_Status status;
    if (G->rank) {
        MPI_Recv(&shift, 1, MPI_LONG_LONG_INT, G->rank - 1, 0, MPI_COMM_WORLD, &status);
        f = fopen(filename, "ab");
        if (f == NULL) {
            MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
        }
    } else {
        f = fopen(filename, "wb");
        if (f == NULL) {
            MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
        }
        
        assert(fwrite(&G->n, sizeof(vertex_id_t), 1, f) == 1);
        assert(fwrite(&G->m, sizeof(edge_id_t), 1, f) == 1);
        unsigned char align = 0;
        assert(fwrite(&align, sizeof(unsigned char), 1, f) == 1);
    }
    
    for (int i = 0; i <= G->local_n; i++) {
        G->rowsIndices[i] += shift;
    }
    
    if (G->rank == 0) {
        assert(fwrite(G->rowsIndices, sizeof(edge_id_t), G->local_n + 1, f) == G->local_n + 1);
    } else {
        assert(fwrite(&G->rowsIndices[1], sizeof(edge_id_t), G->local_n, f) == G->local_n);
    }
    
    for (int i = 0; i <= G->local_n; i++) {
        G->rowsIndices[i] -= shift;
    }
    
    fclose(f);
    
    if (G->nproc != 1) {
        edge_id_t tmp = G->rowsIndices[G->local_n] + shift;
        MPI_Send(&tmp, 1, MPI_LONG_LONG_INT, (G->rank + 1) % G->nproc, 0, MPI_COMM_WORLD);
    }
    
    unsigned long long trash = 0;
    if (G->nproc != 1) {
        MPI_Recv(&trash, 1, MPI_LONG_LONG_INT, (G->rank - 1 + G->nproc) % G->nproc, 0, MPI_COMM_WORLD, &status);
    }
    
    f = fopen(filename, "ab");
    
    assert(fwrite(G->endV, sizeof(vertex_id_t), G->local_m, f) == G->local_m);    
    
    fclose(f);
    
    if (G->rank != G->nproc - 1) {
        MPI_Send(&trash, 1, MPI_LONG_LONG_INT, G->rank + 1, 0, MPI_COMM_WORLD);
    }
}

/* write blackholes in file and stdout, but you can don't use the file output in your solution, it's only for validation */
void write_answer(const vector<vertex_id_t> &all_answers_pool, char *outFilename, int target_blackhole_size)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (!rank) {
        FILE *f = fopen(outFilename, "w");
        if (f == NULL) {
            MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
        }
        
        cout << endl;
        for (int i = 0; i < all_answers_pool.size(); i++) {
            vertex_id_t tmp = all_answers_pool[i];
//            fwrite(&tmp, sizeof(vertex_id_t), 1, f);
            fprintf(f, "%d ", tmp);
            cout << tmp << " " << endl;
            if (i % target_blackhole_size == target_blackhole_size - 1) {
                cout << endl;
            }
        }
        
        fclose(f);
        if (rank != size - 1) {
            int trash = 0;
            MPI_Send(&trash, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
        }
    } else {
        int trash = 0;
        MPI_Status status;
        MPI_Recv(&trash, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
        
        FILE *f = fopen(outFilename, "a");
        if (f == NULL) {
            MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
        }
        
        for (int i = 0; i < all_answers_pool.size(); i++) {
            vertex_id_t tmp = all_answers_pool[i];
//            fwrite(&tmp, sizeof(vertex_id_t), 1, f);
            fprintf(f, "%d ", tmp);
            cout << tmp << " " << endl;
            if (i % target_blackhole_size == target_blackhole_size - 1) {
                cout << endl;
            }
        }
        
        fclose(f);
        
        if (rank != size - 1) {
            int trash = 0;
            MPI_Send(&trash, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
        }
    }
}

/* dfs for check the considered subgraph is connected or not */
int dfs(int v, multimap<vertex_id_t, vertex_id_t> &edges, set<vertex_id_t> &visited_vertices)
{
    int res = 0;
    visited_vertices.insert(v);
    auto neighbors = edges.equal_range(v);
    for (auto it = neighbors.first; it != neighbors.second; ++it) {
        if (visited_vertices.find(it->second) == visited_vertices.end()) {
            res += dfs(it->second, edges, visited_vertices);
        }
    }
    
    return res + 1;
}

/* check blackhole or not? */
bool is_black_hole(graph_t *G, const vector<vertex_id_t> &blackhole_vertices_ids, MPI_Win &win_rowsIndices, MPI_Win &win_endV)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    set<vertex_id_t> vertices;
    for (int i = 0; i < blackhole_vertices_ids.size(); i++) {
        vertices.insert(blackhole_vertices_ids[i]);
    }
    
    /* extract subgraph */
    multimap<vertex_id_t, vertex_id_t> edges;
    for (int i = 0; i < blackhole_vertices_ids.size(); i++) {
        int owner = VERTEX_OWNER(blackhole_vertices_ids[i], G->n, G->nproc);
        if (owner == rank) {
            int local_num = VERTEX_LOCAL(blackhole_vertices_ids[i], G->n, G->nproc, G->rank);
            for (int j = G->rowsIndices[local_num]; j < G->rowsIndices[local_num + 1]; j++) {
                if (vertices.find(G->endV[j]) != vertices.end()) {    
                    edges.insert(make_pair(blackhole_vertices_ids[i], G->endV[j]));
                    edges.insert(make_pair(G->endV[j], blackhole_vertices_ids[i]));
                } else {
                    /* there is out edge from this group of vertices - it isn't blackhole */
                    return false;
                }
            }
        } else {
            pair<vertex_id_t *, unsigned> remote_edges_info = get_remote_edges(G, blackhole_vertices_ids[i], win_rowsIndices, win_endV);
            
            for (int j = 0; j < remote_edges_info.second; j++) {
                if (vertices.find((remote_edges_info.first)[j]) != vertices.end()) {    
                    edges.insert(make_pair(blackhole_vertices_ids[i], (remote_edges_info.first)[j]));
                    edges.insert(make_pair((remote_edges_info.first)[j], blackhole_vertices_ids[i]));
                } else {
                    // there is out edge from this group of vertices
                    delete[] remote_edges_info.first;
                    return false;
                }
            }
            
            delete[] remote_edges_info.first;
        }
    }
    
    /* there aren't out edges, but we need to check it's connected subgraph */
    set<vertex_id_t> visited_vertices;
    if (dfs(blackhole_vertices_ids[0], edges, visited_vertices) == blackhole_vertices_ids.size()) {
        return true;
    }
    
    return false;
}

/* get vertex info (endV array and the size) from remote node */
pair<vertex_id_t *, unsigned> get_remote_edges(graph_t *G, vertex_id_t u, MPI_Win &win_rowsIndices, MPI_Win &win_endV)
{
    int owner = VERTEX_OWNER(u, G->n, G->nproc);
    int local_num = VERTEX_LOCAL(u, G->n, G->nproc, owner);
    edge_id_t left_rowIndices = 0;
    edge_id_t right_rowIndices = 0;
    MPI_Win_lock(MPI_LOCK_SHARED, owner, 0, win_rowsIndices);
    MPI_Get(&left_rowIndices, 1, MPI_LONG_LONG_INT, owner, local_num, 1, MPI_LONG_LONG_INT, win_rowsIndices);
    MPI_Get(&right_rowIndices, 1, MPI_LONG_LONG_INT, owner, local_num + 1, 1, MPI_LONG_LONG_INT, win_rowsIndices);
    MPI_Win_unlock(owner, win_rowsIndices);
    vertex_id_t *buf = new vertex_id_t[right_rowIndices - left_rowIndices];
    MPI_Win_lock(MPI_LOCK_SHARED, owner, 0, win_endV);
    MPI_Get(buf, right_rowIndices - left_rowIndices, MPI_INT, owner, left_rowIndices, right_rowIndices - left_rowIndices, MPI_INT, win_endV);
    MPI_Win_unlock(owner, win_endV);
    return make_pair(buf, right_rowIndices - left_rowIndices);
}

#define MOD_SIZE(v) ((v) % size)
#define DIV_SIZE(v) ((v) / size)
#define MUL_SIZE(x) ((x) * size)

/* returns number of vertex owner, v - the global vertex number, TotVertices - the global number of vertices, size - the number of processes */
int VERTEX_OWNER(const vertex_id_t v, const vertex_id_t TotVertices, const int size)
{
    vertex_id_t mod_size = MOD_SIZE(TotVertices);
    vertex_id_t div_size = DIV_SIZE(TotVertices);
    if (!mod_size) {
        return v / div_size;
    } else {
        if (v / (div_size + 1) < mod_size) {
            return v / (div_size + 1);
        } else {
            return (v - mod_size * (div_size + 1)) / div_size + mod_size;
        }
    }
}

/* returns local vertex number, v - the global vertex number, TotVertices - the global number of vertices, size - the number of processes, rank - the process number */
vertex_id_t VERTEX_LOCAL(const vertex_id_t v, const vertex_id_t TotVertices, const int size, const int rank) 
{
    if (MOD_SIZE(TotVertices) <= (unsigned int)rank) {
        return ((v - MOD_SIZE(TotVertices) * (DIV_SIZE(TotVertices) + 1)) % DIV_SIZE(TotVertices));
    } else {
        return (v % (DIV_SIZE(TotVertices) + 1));
    }
}

/* returns global vertex number, v_local - the local vertex number, TotVertices - the global number of vertices, size - the number of processes, rank - the process number */
vertex_id_t VERTEX_TO_GLOBAL(const vertex_id_t v_local, const vertex_id_t TotVertices, const int size, const int rank)
{
    if (MOD_SIZE(TotVertices) > (unsigned int)rank) {
        return ((DIV_SIZE(TotVertices) + 1) * rank + (vertex_id_t)v_local);
    } else {
        return (MOD_SIZE(TotVertices) * (DIV_SIZE(TotVertices) + 1) + DIV_SIZE(TotVertices) * (rank - MOD_SIZE(TotVertices)) + v_local);
    }
}

/* calculate local vertex count (is using in distributed graph generators) */
vertex_id_t get_local_n(graph_t *G)
{
    vertex_id_t TotVertices = G->n;
    unsigned size = G->nproc;
    unsigned rank = G->rank;
    vertex_id_t mod_size = MOD_SIZE(TotVertices);
    vertex_id_t div_size = DIV_SIZE(TotVertices);
    if (!mod_size) {
        return div_size;
    } else {
        if (rank < mod_size) {
            return div_size + 1;
        } else {
            return div_size;
        }
    }
}

