#include "defs.h"
using namespace std;

/* cnt of different unique sets from elem_cnt elements for array from n elements */
int get_perm_cnt(int n, int elem_cnt)
{
    unsigned long long perm_cnt = 1;
    unsigned long long fac = 1;
    for (int i = 0; i < elem_cnt; i++) {
        if (perm_cnt > (unsigned long long)numeric_limits<int>::max()) {
            return min(perm_cnt / fac, (unsigned long long)numeric_limits<int>::max());
        }
        perm_cnt *= (n - i);
        fac *= (i + 1);
    }
    
    perm_cnt /= fac;
    return min(perm_cnt, (unsigned long long)numeric_limits<int>::max());
}

/* get blackhole ids of vertices for permutation "perm_num" */
void get_perm(graph_t *G, unsigned long long perm_num, vector<vertex_id_t> &blackhole_vertices_ids, int last_vertex, int target_blackhole_size)
{
    if (blackhole_vertices_ids.size() == target_blackhole_size) {
        return;
    }
    
    unsigned long long sum_cnt = 0;
    for (int i = last_vertex + 1; i < G->n; i++) {
        if (perm_num < sum_cnt + get_perm_cnt(G->n - 1 - i, target_blackhole_size - blackhole_vertices_ids.size() - 1)) {
            blackhole_vertices_ids.push_back(i);
            get_perm(G, perm_num - sum_cnt, blackhole_vertices_ids, i, target_blackhole_size);
            return;
        }
        
        sum_cnt += get_perm_cnt(G->n - 1 - i, target_blackhole_size - blackhole_vertices_ids.size() - 1);
    }
}

/* brute force */
int run(graph_t *G, const int &target_blackhole_size, const char * outFilename)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Status status;
    MPI_File bh_file;
    MPI_File_open(MPI_COMM_WORLD, outFilename, MPI_MODE_WRONLY|MPI_MODE_CREATE, MPI_INFO_NULL, &bh_file);
    
    vector<vertex_id_t> blackhole_vertices_ids;
    string bh_to_write;
    blackhole_vertices_ids.reserve(target_blackhole_size);
    
    /* get total number of subgraphs with size "target_blackhole_size" */
    /* we use permutations, permutation - vertex ids; every permutation have unique id */
    int perm_cnt = get_perm_cnt(G->n, target_blackhole_size);
    /* for instance, if target_blackhole_size = 3 --> [0, 1, 2] -> permutation 0 */
    /*                                                [0, 1, 3] -> permutation 1 */
    /*                                                ...                        */
    /*                                                [0, 1, n] -> permutation n */
    
    //cout << "total permutations cnt = " << perm_cnt << endl;
    
    /* calculate job amount per one process */
    int perm_per_proc = perm_cnt / size;
    
    int first_perm = (rank < perm_cnt % size) ? (rank * (perm_per_proc + 1)) : (perm_per_proc * rank + perm_cnt % size);
    int last_perm = first_perm + (rank < perm_cnt % size ? (perm_per_proc + 1) : (perm_per_proc)) - 1;
    
    MPI_Win win_rowsIndices, win_endV;
    
    /* open some data for other processes (remote memory access) */
    MPI_Win_create(G->rowsIndices, (G->n / G->nproc + 2) * sizeof(edge_id_t), sizeof(edge_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_rowsIndices);
    MPI_Win_fence(0, win_rowsIndices);
    
    int max_edges_per_node = 0;
    MPI_Allreduce(&G->local_m, &max_edges_per_node, 1, MPI_LONG_LONG_INT, MPI_MAX, MPI_COMM_WORLD);
    
    MPI_Win_create(G->endV, max_edges_per_node * sizeof(vertex_id_t), sizeof(vertex_id_t), MPI_INFO_NULL, MPI_COMM_WORLD, &win_endV);
    MPI_Win_fence(0, win_endV);
    
    int local_ans = 0;
    for (int i = first_perm; i <= last_perm; i++) {
        blackhole_vertices_ids.clear();
        /* extract current blackhole vertex ids from permutation number */
        get_perm(G, i, blackhole_vertices_ids, -1, target_blackhole_size);
        /* check blackhole or not */
        if (is_black_hole(G, blackhole_vertices_ids, win_rowsIndices, win_endV)) {
            local_ans++;
            /* write blackhole to file */
            for (int j = 0; j < blackhole_vertices_ids.size(); j++) {
                bh_to_write += to_string(blackhole_vertices_ids[j]) + ' ';
            }
            MPI_File_write_shared(bh_file,  bh_to_write.data(), bh_to_write.length(), MPI_CHAR, &status);
            bh_to_write.clear();
        }
    }
    
    int ans = 0;
    MPI_Reduce(&local_ans, &ans, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    
    /*
    if (rank == 0) {
        cout << "ans = " << ans << endl;
    }
    */
    
    MPI_Win_free(&win_rowsIndices);
    MPI_Win_free(&win_endV);
    MPI_File_close(&bh_file);
    // it isn't necessary to return number of founded blackholes, just example...
    return ans;
}
