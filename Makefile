#defines
MPICXX=mpicxx
SPEEDFLAGS=-O3
CXXFLAGS=$(SPEEDFLAGS) -std=c++11
LDFLAGS=-lrt $(SPEEDFLAGS)

TARGET = gen_random gen_random_mpi reference_mpi solution_mpi validation

all: $(TARGET)

# your own distributed implementation
solution_mpi: main_mpi.o solution_mpi.o tools.o
	$(MPICXX) $^ -o $@ $(LDFLAGS)

# distributed reference implementation
reference_mpi: main_mpi.o reference_mpi.o tools.o
	$(MPICXX) $^ -o $@ $(LDFLAGS)

# Erdos-Renyi (random) graph generator
gen_random: gen_random.o tools.o
	$(MPICXX) $^ -o $@ $(LDFLAGS)

# Erdos-Renyi (random) distributed graph generator
gen_random_mpi: gen_random_mpi.o tools.o
	$(MPICXX) $^ -o $@ $(LDFLAGS)

# validation
validation: validation.o tools.o
	$(MPICXX) $^ -o $@ $(LDFLAGS)

.cpp.o:
	$(MPICXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -rf *.o $(TARGET)
