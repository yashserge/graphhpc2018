#include "defs.h"
#include <chrono>

using namespace std;

char inFilename[FILENAME_LEN];
char outFilename[FILENAME_LEN];

int nIters = 1;

/* helper */
void usage(int argc, char **argv)
{
    printf("Usage:\n");
    printf("%s -in <input> [other options]\n", argv[0]);
    printf("Options:\n");
    printf("    -in <input> -- input graph filename\n");
    printf("    -blackhole_size <target_blackhole_size> -- number of vertices in target blackhole subgraph\n");
    printf("    -nIters <nIters> -- number of iterations\n");
	printf("    -out <output filename>, file for the result storage\n");
    MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
}

/* initialization */
void init(int argc, char **argv, graph_t *G, int &target_blackhole_size)
{
    MPI_Init(&argc, &argv);
    bool should_read_graph = false;
    MPI_Comm_size(MPI_COMM_WORLD, &G->nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &G->rank);
    
    if (argc == 1) {
        usage(argc, argv);
    }
    
	bool no_outFilename = true;
	
    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-in")) {
            should_read_graph = true;
            strncpy(inFilename, argv[++i], sizeof(inFilename));
        }
        
        if (!strcmp(argv[i], "-blackhole_size")) {
            target_blackhole_size = (int)atoi(argv[++i]);
        }
        
        if (!strcmp(argv[i], "-nIters")) {
            nIters = (int)atoi(argv[++i]);
        }
		
		if (!strcmp(argv[i], "-out")) {
            no_outFilename = false;
            sprintf(outFilename, "%s", argv[++i]);
        }
    }
    
    if (target_blackhole_size < 2) {
        usage(argc, argv);
    }
    
    if (should_read_graph) {
        readGraph_singleFile_MPI(G, inFilename);
    } else {
        usage(argc, argv);
    }
    
    if (no_outFilename) {
        sprintf(outFilename, "%s.res", inFilename);
    }
}

void func_time_control()
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    this_thread::sleep_for(chrono::minutes(1));
    if (!rank) cout << "abort after 1 min." << endl;
    std::quick_exit(EXIT_SUCCESS);
//    std::terminate();
    MPI_Abort(MPI_COMM_WORLD, MPI_SUCCESS);
}

int main(int argc, char **argv)
{
    graph_t g;
    int target_blackhole_size = 0;
    double start_ts, finish_ts;
    double *alg_time;
    /* initializing */
    
    //auto start = std::chrono::high_resolution_clock::now();    
    init(argc, argv, &g, target_blackhole_size);
    //MPI_Barrier(MPI_COMM_WORLD);
    //auto finish = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed = finish - start;
    //if (g.rank == 0) std::cout << "ReadGraph time: " << elapsed.count() << " s\n";
    
    alg_time = new double[nIters];
    assert(alg_time != NULL);
    
    //printGraph(&g);
    for (int i = 0; i < nIters; i++) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (g.rank != -1) {
            thread time_controller(func_time_control);
            time_controller.detach();
        }
        
        start_ts = MPI_Wtime();
        int blackHolesCnt = run(&g, target_blackhole_size, outFilename);
        MPI_Barrier(MPI_COMM_WORLD);
        finish_ts = MPI_Wtime();
        double time = finish_ts - start_ts;
        alg_time[i] = time;
        //if (g.rank == 0) {
        //    cout.precision(5);
        //    cout << " finished. Time is " << fixed << time << " sec." << endl;
        //}
    }
    
    /* final print */
    double min_time, max_time, avg_time;
    double global_min_time, global_max_time, global_avg_time;
    max_time = avg_time = 0;
    min_time = DBL_MAX;     
    for (int i = 0; i < nIters; ++i) {  
        avg_time += alg_time[i];
        if (alg_time[i] < min_time) {
            min_time = alg_time[i];
        }
        
        if (alg_time[i] > max_time) {
            max_time = alg_time[i];
        }
    }
    
    avg_time /= nIters;
    
    MPI_Reduce(&min_time, &global_min_time, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
    MPI_Reduce(&max_time, &global_max_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&avg_time, &global_avg_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    //if (g.rank == 0) {
        //cout << inFilename << " vertices = " << g.n << ", edges = " << g.m << ", nIters = " << nIters 
        //    << ", time: min = " << global_min_time << ", avg = " << global_avg_time / g.nproc << ", max = " << global_max_time << endl;
        // print average time
        //cout << "Time = " << global_avg_time / g.nproc << " sec." << endl;
    //}
    
    delete[] alg_time;
    freeGraph(&g);
    
    MPI_Finalize();
    return 0;
}
